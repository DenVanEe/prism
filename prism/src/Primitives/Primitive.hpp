#pragma once

#include <memory>

#include "../math/Bound.hpp"
#include "../math/Ray.hpp"
#include "../math/Interaction.hpp"
#include "../math/AnimatedTransform.hpp"
#include "../math/Transform.hpp"

namespace prism
{
	class AreaLight;
	class MemoryArena;

	class Material
	{
	public:
		void computeScatteringFunctions(SurfaceInteraction* surfInt, MemoryArena& arena, TransportMode mode, bool allowMultipleLobes) const;
	};

	class TransportMode
	{

	};

	class Primitive
	{
	public:
		virtual Bound3 worldBound() const = 0;
		virtual bool intersect(const Ray& ray, SurfaceInteraction* surfInt) const = 0;
		virtual bool intersectP(const Ray& ray) const = 0;
		virtual const AreaLight* getAreaLight() const = 0;
		virtual const Material* getMaterial() const = 0;
		virtual void computeScatteringFunctions(SurfaceInteraction* surfInt, MemoryArena& arena, TransportMode mode, bool allowMultipleLobes) const = 0;

	private:

	};

	class GeometricPrimitive : public Primitive
	{
	public:
		GeometricPrimitive(std::shared_ptr<Shape> shape, std::shared_ptr<Material> material, MediumInterface mediumInterface, std::shared_ptr<AreaLight> areaLight = nullptr) :
			m_shape(shape),
			m_material(material),
			m_areaLight(areaLight),
			m_mediumInterface(mediumInterface)
		{
		}

		Bound3 worldBound() const override
		{
			return m_shape->worldBound();
		}

		bool intersect(const Ray& ray, SurfaceInteraction* surfInt) const override
		{
			Float tHit;
			if (!m_shape->intersect(ray, &tHit, surfInt))
			{
				return false;
			}

			ray.tMax = tHit;
			surfInt->primitive = this;
			// TODO: this will get done in the future
			return true;
		}

		bool intersectP(const Ray& ray) const override
		{
			return m_shape->intersectP(ray);
		}

		const AreaLight* getAreaLight() const
		{
			return m_areaLight.get();
		}

		void computeScatteringFunctions(SurfaceInteraction* surfInt, MemoryArena& arena, TransportMode mode, bool allowMultipleLobes) const override
		{
			if (m_material)
			{
				m_material->computeScatteringFunctions(surfInt, arena, mode, allowMultipleLobes);
			}
		}

	private:
		std::shared_ptr<Shape>		m_shape;
		std::shared_ptr<Material>   m_material;
		std::shared_ptr<AreaLight>  m_areaLight;

		MediumInterface m_mediumInterface;
	};

	class TransformedPrimitive : public Primitive
	{
	public:
		TransformedPrimitive(std::shared_ptr<Primitive> primitive, AnimatedTransform primitiveToWorld) :
			m_primitive(primitive),
			m_primitiveToWorld(primitiveToWorld)
		{
		}

		bool intersect(const Ray& ray, SurfaceInteraction* surfInt) const
		{
			// interpolate and transform ray to primitive's coordinate system
			const Transform interpolatedPrimitiveToWorld = m_primitiveToWorld.interpolate(ray.time);
			const Ray intRay = inverse(interpolatedPrimitiveToWorld).ray(ray);

			if (!m_primitive->intersect(ray, surfInt))
			{
				return false;
			}

			if (!interpolatedPrimitiveToWorld.i)

			ray.tMax = intRay.tMax;
			return true;
		}

	private:
		std::shared_ptr<Primitive> m_primitive;

		// consider this primitive to world
		// and if a primitive has it's own transformation, it would be object to primitive.
		// so overal, you need both to go from object to world.
		const AnimatedTransform m_primitiveToWorld;
	};
}