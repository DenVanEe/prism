#pragma once

#include <memory>
#include <vector>

#include "../math/math.hpp"
#include "../math/Transform.hpp"

namespace prism
{
	template<class T>
	class Texture;

	class TriangleMesh
	{
	public:
		TriangleMesh(const Transform& objectToWorld, int nTriangles, const int* vertexIndices, int nVertices, 
			const Vec3* P, const Vec3* N, const Vec3* S, const Vec2* UV, std::shared_ptr<Texture<Float>> alphaMask) :
			nTriangles(nTriangles), nVertices(nVertices),
			vertexIndices(vertexIndices, vertexIndices + 3 * nVertices),
			alphaMask(alphaMask),
			p(std::make_unique<Vec3[]>(nVertices)),
			n(nullptr),
			s(nullptr),
			uv(nullptr)
		{
			// transform to world space:
			for (int i = 0; i < nVertices; i++)
			{
				p[i] = objectToWorld.pnt(P[i]);
			}

			if (N)
			{
				n.reset(new Vec3[nVertices]);
				for (int i = 0; i < nVertices; i++)
				{
					n[i] = objectToWorld.nrm(N[i]);
				}
			}

			if (S)
			{
				s.reset(new Vec3[nVertices]);
				for (int i = 0; i < nVertices; i++)
				{
					s[i] = objectToWorld.vec(S[i]);
				}
			}

			if (UV)
			{
				Vec2* tuv = new Vec2[nVertices];
				std::copy(UV, UV + nVertices, tuv);
				uv.reset(tuv);
			}
		}

	public:
		const int nTriangles, nVertices;
		const std::vector<int> vertexIndices;
		std::unique_ptr<Vec3[]> p;
		std::unique_ptr<Vec3[]> n;
		std::unique_ptr<Vec3[]> s;
		std::unique_ptr<Vec2[]> uv;
		std::shared_ptr<Texture<Float>> alphaMask;
	};
}