#pragma once

#include "../math/math.hpp"
#include "../math/Transform.hpp"

namespace prism
{
    class Shape
    {
	public:
		Shape(const Transform* objToWorld, const Transform* worldToObj, bool reverseOrientation) :
			reverseOrientation(reverseOrientation),
			transSwapsHandedness(objToWorld->swapsHandedness()),
			m_objToWorld(objToWorld),
			m_worldToObj(worldToObj)
		{
		}

		virtual Bound3 objectBound() const = 0;
		virtual Bound3 worldBound() const
		{
			return m_objToWorld->bnd(objectBound());
		}

		virtual bool intersect(Ray ray, Float* tHit, SurfaceInteraction* surfInt, bool testAlphaTexture = true) const = 0;
		virtual bool intersectP(Ray ray, bool testAlphaTexture = true) const
		{
			Float tHit = ray.tMax;
			SurfaceInteraction surfInt;
			return intersect(ray, &tHit, &surfInt, testAlphaTexture);
		}

		virtual Float surfaceArea() const = 0;

	public:
		const bool reverseOrientation;
		const bool transSwapsHandedness;

	protected:
		const Transform* m_objToWorld;
		const Transform* m_worldToObj;
    };
}