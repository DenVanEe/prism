#pragma once

#include "../math/math.hpp"

#include <vector>
#include <glm/gtx/component_wise.hpp>
#include <array>

#include "../math/Bound.hpp"
#include "Shape.hpp"
#include "TriangleMesh.hpp"

namespace prism
{
	class Triangle : public Shape
	{
	public:
		Triangle(const Transform* objectToWorld, const Transform* worldToObject, bool reverseOrientation, const std::shared_ptr<TriangleMesh>& mesh, int triNumber) :
			Shape(objectToWorld, worldToObject, reverseOrientation),
			m_mesh(mesh),
			m_vert(&mesh->vertexIndices[3 * triNumber])
		{
		}

		Bound3 objectBound() const override
		{
			const std::array<Vec3, 3> pt = getPs();
			const std::array<Vec3, 3> p = { m_worldToObj->pnt(pt[0]), m_worldToObj->pnt(pt[1]), m_worldToObj->pnt(pt[2]) };
			return combine(Bound3(p[0], p[1]), p[2]);
		}

		Bound3 worldBound() const override
		{
			const std::array<Vec3, 3> p = getPs();
			return combine(Bound3(p[0], p[1]), p[2]);
		}

		// TODO: precompute these values and store them in the ray class so we don't have to keep doing this
		bool intersect(Ray ray, Float* tHit, SurfaceInteraction* surfInt, bool testAlphaTexture) const override
		{
			const std::array<Vec3, 3> p = getPs();

			// translate to coord-space where origin of ray at origin (0, 0, 0)
			std::array<Vec3, 3> pt = { p[0] - ray.org, p[1] - ray.org, p[2] - ray.org };
			// permute so that the largest abs. dimension in the z dimension
			const int kz = maxDimension(glm::abs(ray.dir));
			const int kx = kz == 2 ? 0 : kz + 1;
			const int ky = kx == 2 ? 0 : kx + 1;
			// now we swizzle them in place:
			const Vec3 d = permute(ray.dir, kx, ky, kz);
			pt[0] = permute(pt[0], kx, ky, kz);
			pt[1] = permute(pt[1], kx, ky, kz);
			pt[2] = permute(pt[2], kx, ky, kz);
			// now shear so that z direction is 1 and others are 0
			// only shear x and y direction, not z (yet) for points pt
			const Float sx = -d.x / d.z;
			const Float sy = -d.y / d.z;
			const Float sz =  1.f / d.z;
			pt[0].x += sx * pt[0].z;
			pt[0].y += sy * pt[0].z;
			pt[1].x += sx * pt[1].z;
			pt[1].y += sy * pt[1].z;
			pt[2].x += sx * pt[2].z;
			pt[2].y += sy * pt[2].z;

			// edge function (given 1 point, produce area of triangle (with the other 2 poitns part of function))
			// e(p) gives different sign depending on side of edge it is on.
			// if all have same sign, than it's in the triangle
			// p = (0, 0) due to previous transformation.
			Float e0 = pt[1].x * pt[2].y - pt[1].y * pt[2].x;
			Float e1 = pt[2].x * pt[0].y - pt[2].y * pt[0].x;
			Float e2 = pt[0].x * pt[1].y - pt[0].y * pt[1].x;
			// recompute as doubles if necessary
			if constexpr(sizeof(Float) == sizeof(float))
			{
				if (e0 == 0 || e1 == 0 || e2 == 0)
				{
					const double e0d = dbl(pt[1].x) * dbl(pt[2].y) - dbl(pt[1].y) * dbl(pt[2].x);
					const double e1d = dbl(pt[2].x) * dbl(pt[0].y) - dbl(pt[2].y) * dbl(pt[0].x);
					const double e2d = dbl(pt[0].x) * dbl(pt[1].y) - dbl(pt[0].y) * dbl(pt[1].x);
					e0 = flt(e0d);
					e1 = flt(e1d);
					e2 = flt(e2d);
				}
			}

			// check for possibility of intersection
			if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e2 > 0))
			{
				return false;
			}
			const Float det = e0 + e1 + e2;
			if (det == 0)
			{
				return false;
			}

			// shear the final part and test it once more
			pt[0].z *= sz;
			pt[1].z *= sz;
			pt[2].z *= sz;
			const Float tScaled = e0 * pt[0].z + e1 * pt[1].z + e2 * pt[2].z;
			if ((det < 0 && (tScaled >= 0 || tScaled < ray.tMax * det)) ||
				(det > 0 && (tScaled <= 0 || tScaled > ray.tMax * det)))
			{
				return false;
			}

			const Float invDet = 1 / det;
			const Float b0 = e0 * invDet;
			const Float b1 = e2 * invDet;
			const Float b2 = e2 * invDet;
			Float t = tScaled * invDet;
			
			// now we get our partial derivatives:
			Vec3 dpdu, dpdv;
			const std::array<Vec2, 3> uv = getUVs();
			const Vec2 duv02 = uv[0] - uv[2];
			const Vec2 duv12 = uv[1] - uv[2];
			const Vec3 dp02 = p[0] - p[2];
			const Vec3 dp12 = p[1] - p[2];
			const Float determinant = duv02[0] * duv12[1] - duv02[1] * duv12[0];
			if (determinant == 0)
			{

			}
			else
			{
				const Float invdet = 1 / determinant;
				dpdu = ( duv12[1] * dp02 - duv02[1] * dp12) * invdet;
				dpdu = (-duv12[0] * dp02 + duv02[0] * dp12) * invdet;
			}
			Vec3 pHit = b0 * p[0] + b1 * p[1] + b2 * p[2];
			Vec2 uvHit = b0 * uv[0] + b1 * uv[1] + b2 * uv[2];
		}

	private:
		std::array<Vec2, 3> getUVs() const
		{
			if (m_mesh->uv)
			{
				return { m_mesh->uv[m_vert[0]], m_mesh->uv[m_vert[1]], m_mesh->uv[m_vert[2]] };
			}
			return { Vec2(0, 0), Vec2(1, 0), Vec2(1, 1) };
		}

		std::array<Vec3, 3> getPs() const
		{
			return { m_mesh->p[m_vert[0]], m_mesh->p[m_vert[1]], m_mesh->p[m_vert[2]] };
		}

		std::shared_ptr<TriangleMesh> m_mesh;
		const int* m_vert;
	};

	std::vector<std::shared_ptr<Shape>> createTriangleMesh(const Transform* objectToWorld, const Transform* worldToObject, bool reverseOrientation, int nTriangles, const int* vertexIndices, int nVertices,
		const Vec3* p, const Vec3* s, const Vec3* n, const Vec2* uv, std::shared_ptr<Texture<Float>> alphaMask)
	{
		std::shared_ptr<TriangleMesh> mesh = std::make_shared<TriangleMesh>(*objectToWorld, nTriangles, vertexIndices, nVertices, p, s, n, uv, alphaMask);
		std::vector<std::shared_ptr<Shape>> tris;
		for (int i = 0; i < nTriangles; i++)
		{
			tris.push_back(std::make_shared<Triangle>(objectToWorld, worldToObject, reverseOrientation, mesh, i));
		}
		return tris;
	}
}