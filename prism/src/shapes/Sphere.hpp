#pragma once

#include "../math/math.hpp"
#include "../math/Floating.hpp"
#include "Shape.hpp"

namespace prism
{
	class Sphere : public Shape
	{
	public:
		Sphere(const Transform* objToWorld, const Transform* worldToObj, bool reverseOrientation, Float rad, Float zMin, Float zMax, Float phiMax) :
			Shape(objToWorld, worldToObj, reverseOrientation),
			m_radius(rad),
			m_zMin(glm::clamp(std::min(zMin, zMax), -rad, rad)),
			m_zMax(glm::clamp(std::max(zMin, zMax), -rad, rad)),
			m_thetaMin(std::acos(glm::clamp(zMin / rad, F(-1), F(1)))),
			m_thetaMax(std::acos(glm::clamp(zMax / rad, F(-1), F(1)))),
			m_phiMax(glm::radians(glm::clamp(phiMax, F(0), F(36))))
		{
		}

		Bound3 objectBound() const override
		{
			static constexpr Float ref[3] = { hlfPI<Float>(), pi<Float>(), 3 * hlfPI<Float>() };

			if (m_phiMax > ref[2])
			{
				const Vec3 pMin(-m_radius, -m_radius, m_zMin);
				const Vec3 pMax(m_radius, m_radius, m_zMax);
				return Bound3(pMin, pMax);
			}

			const Float phiCos = std::cos(m_phiMax);
			const Float phiSin = std::sin(m_phiMax);

			if (m_phiMax < ref[0])
			{
				const Vec3 pMin(phiCos, 0, m_zMin);
				const Vec3 pMax(m_radius, phiSin, m_zMax);
				return Bound3(pMin, pMax);
			}
			else if (m_phiMax < ref[1])
			{
				const Vec3 pMin(phiCos, 0, m_zMin);
				const Vec3 pMax(m_radius, m_radius, m_zMax);
				return Bound3(pMin, pMax);
			}
			else
			{
				const Vec3 pMin(-m_radius, phiSin, m_zMin);
				const Vec3 pMax(m_radius, m_radius, m_zMax);
				return Bound3(pMin, pMax);
			}
		}

		bool intersectP(Ray r, bool testAlphaTexture = true) const override
		{
			Float phi;
			Vec3 pHit;

			// Transform ray to object space
			Vec3 oErr, dErr;
			const Ray ray = m_worldToObj->ray(r, &oErr, &dErr);
			// Origin and Direction error
			ERay rayx;
			rayx.org = EVec3(EFloat(ray.org.x, oErr.x), EFloat(ray.org.y, oErr.y), EFloat(ray.org.z, oErr.z));
			rayx.dir = EVec3(EFloat(ray.dir.x, dErr.x), EFloat(ray.dir.y, dErr.y), EFloat(ray.dir.z, dErr.z));

			// quadratic form of the equation (that is, x^2 + y^2 + z^2 = r)
			const EFloat a = glm::length2(rayx.dir);
			const EFloat b = F(2) * glm::dot(rayx.dir, rayx.org);
			const EFloat c = glm::length2(rayx.org) - EFloat(m_radius) * EFloat(m_radius);

			EFloat t0, t1;
			if (!quadratic(a, b, c, &t0, &t1))
			{
				return false;
			}

			// quick check to see if it's out of range
			if (t0.upperBound() > ray.tMax || t1.lowerBound() <= 0)
			{
				return false;
			}

			EFloat tShapeHit = t0;
			// it could be the lower bound t0
			if (tShapeHit.lowerBound <= 0)
			{
				tShapeHit = t1;
				// or it could be upper bound t1
				if (tShapeHit.upperBound() > ray.tMax)
				{
					return false;
				}
			}

			pHit = ray(Float(tShapeHit));
			// refine the intersection point (pg 225 pbrt)
			pHit *= m_radius / glm::distance(pHit, Vec3(0.f));
			if (pHit.x == 0 && pHit.y == 0)
			{
				pHit.x = 1e-5f * m_radius;
			}

			phi = std::atan2(pHit.y, pHit.x);
			// adjust it to the correct range:
			if (phi < 0)
			{
				phi += 2 * pi<Float>();
			}

			// check if a modified sphere works:
			if ((m_zMin > -m_radius && pHit.z < m_zMin) || (m_zMax < m_radius && pHit.z > m_zMax) || phi > m_phiMax)
			{
				// if it doesn't and tShapeHit is already t1, than we're done
				if ((tShapeHit == t1) || (t1.upperBound > ray.tMax))
				{
					return false;
				}

				// otherwise we try it with t1
				tShapeHit = t1;

				// reapply the intersection and refine the pHit value again
				pHit = ray(Float(tShapeHit));
				pHit *= m_radius / glm::distance(pHit, Vec3(0.f));
				if (pHit.x == 0 && pHit.y == 0)
				{
					pHit.x = 1e-5f * m_radius;
				}

				phi = std::atan2(pHit.y, pHit.x);
				if (phi < 0)
				{
					phi += 2 * pi<Float>();
				}

				if ((m_zMin > -m_radius && pHit.z < m_zMin) || (m_zMax < m_radius && pHit.z > m_zMax) || phi > m_phiMax)
				{
					return false;
				}
			}
			return true;
		}

		bool intersect(Ray r, Float* tHit, SurfaceInteraction* surfInt, bool testAlphaTexture = true) const override
		{
			Float phi;
			Vec3 pHit;

			// Transform ray to object space
			Vec3 oErr, dErr;
			const Ray ray = m_worldToObj->ray(r, &oErr, &dErr);
			// Origin and Direction error
			ERay rayx;
			rayx.org = EVec3(EFloat(ray.org.x, oErr.x), EFloat(ray.org.y, oErr.y), EFloat(ray.org.z, oErr.z));
			rayx.dir = EVec3(EFloat(ray.dir.x, dErr.x), EFloat(ray.dir.y, dErr.y), EFloat(ray.dir.z, dErr.z));

			// quadratic form of the equation (that is, x^2 + y^2 + z^2 = r)
			const EFloat a = glm::length2(rayx.dir);
			const EFloat b = F(2) * glm::dot(rayx.dir, rayx.org);
			const EFloat c = glm::length2(rayx.org) - EFloat(m_radius) * EFloat(m_radius);
			
			EFloat t0, t1;
			if (!quadratic(a, b, c, &t0, &t1))
			{
				return false;
			}

			// quick check to see if it's out of range
			if (t0.upperBound() > ray.tMax || t1.lowerBound() <= 0)
			{
				return false;
			}

			EFloat tShapeHit = t0;
			// it could be the lower bound t0
			if (tShapeHit.lowerBound <= 0)
			{
				tShapeHit = t1;
				// or it could be upper bound t1
				if (tShapeHit.upperBound() > ray.tMax)
				{
					return false;
				}
			}

			pHit = ray(Float(tShapeHit));
			// refine the intersection point (pg 225 pbrt)
			pHit *= m_radius / glm::distance(pHit, Vec3(0.f));
			if (pHit.x == 0 && pHit.y == 0)
			{
				pHit.x = 1e-5f * m_radius;
			}

			phi = std::atan2(pHit.y, pHit.x);
			// adjust it to the correct range:
			if (phi < 0)
			{
				phi += 2 * pi<Float>();
			}

			// check if a modified sphere works:
			if ((m_zMin > -m_radius && pHit.z < m_zMin) || (m_zMax < m_radius && pHit.z > m_zMax) || phi > m_phiMax)
			{
				// if it doesn't and tShapeHit is already t1, than we're done
				if ((tShapeHit == t1) || (t1.upperBound > ray.tMax))
				{
					return false;
				}

				// otherwise we try it with t1
				tShapeHit = t1;

				// reapply the intersection and refine the pHit value again
				pHit = ray(Float(tShapeHit));
				pHit *= m_radius / glm::distance(pHit, Vec3(0.f));
				if (pHit.x == 0 && pHit.y == 0)
				{
					pHit.x = 1e-5f * m_radius;
				}

				phi = std::atan2(pHit.y, pHit.x);
				if (phi < 0)
				{
					phi += 2 * pi<Float>();
				}

				if ((m_zMin > -m_radius && pHit.z < m_zMin) || (m_zMax < m_radius && pHit.z > m_zMax) || phi > m_phiMax)
				{
					return false;
				}
			}

			const Float u = phi / m_phiMax;
			const Float theta = std::acos(glm::clamp(pHit.z / m_radius, F(-1), F(1)));
			const Float v = (theta - m_thetaMin) / (m_thetaMax - m_thetaMin);

			const Float zRadius = glm::length(Vec2(pHit));
			const Float invZRadius = 1 / zRadius;
			const Float cosPhi = pHit.x * invZRadius;
			const Float sinPhi = pHit.y * invZRadius;
			const Vec3  dpdu = Vec3(-m_phiMax * pHit.y, m_phiMax * pHit.x, F(0));
			const Vec3  dpdv = (m_thetaMax - m_thetaMin) * Vec3(pHit.z * cosPhi, pHit.z * sinPhi, -m_radius * std::sin(theta));

			// take math 120a to understand this
			const Vec3  d2Pduu = -m_phiMax * m_phiMax * Vec3(pHit.x, pHit.y, 0);
			const Vec3  d2Pduv = (m_thetaMax - m_thetaMin) * pHit.z * m_phiMax * Vec3(-sinPhi, cosPhi, F(0));
			const Vec3  d2Pdvv = -(m_thetaMax - m_thetaMin) * (m_thetaMax - m_thetaMin) * pHit;
			const Float E = glm::dot(dpdu, dpdu);
			const Float F = glm::dot(dpdu, dpdv);
			const Float G = glm::dot(dpdv, dpdv);
			const Vec3  N = glm::normalize(glm::cross(dpdu, dpdv));
			const Float e = glm::dot(N, d2Pduu);
			const Float f = glm::dot(N, d2Pduv);
			const Float g = glm::dot(N, d2Pdvv);

			const Float invEGF2 = 1 / (E * G - F * F);
			const Vec3  dndu = Vec3((f * F - e * G) * invEGF2 * dpdu + (e * F - f * E) * invEGF2 * dpdv);
			const Vec3  dndv = Vec3((g * F - f * G) * invEGF2 * dpdu + (f * F - g * E) * invEGF2 * dpdv);

			const Vec3 pError = gamma<Float>(5) * glm::abs(pHit);

			*surfInt = m_objToWorld->snt(SurfaceInteraction(pHit, pError, Vec2(u, v), -ray.dir, dpdu, dpdv, dndu, dndv, ray.time, this));
			*tHit = Float(tShapeHit);
			return true;
		}

		Float surfaceArea() const override
		{
			return m_phiMax * m_radius * (m_zMax - m_zMin);
		}

	private:
		const Float m_radius;
		const Float m_zMin, m_zMax;
		const Float m_thetaMin, m_thetaMax, m_phiMax;
	};
}