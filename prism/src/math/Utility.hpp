#pragma once

#include "math.hpp"

#include <algorithm>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>

#include "Floating.hpp"

namespace prism
{
    // For some reason glm doesn't allow this... (or I can't find it)

	template<typename T>
	glm::tvec3<T> alignDirection(glm::tvec3<T> leader, glm::tvec3<T> follower)
	{
		return glm::dot(leader, follower) > 0 ? follower : -follower;
	}

    template<typename T>
    glm::tmat4x4<T> lerp(glm::tmat4x4<T> m0, glm::tmat4x4<T> m1, T dt)
    {
        glm::tmat4x4<T> result;
        for (int i = 0; i < 4; i++)
        {
            result[i] = glm::lerp(m0[i], m1[i], dt);
        }
        return result;
    }

    template<typename T>
    glm::tmat3x3<T> lerp(glm::tmat3x3<T> m0, glm::tmat3x3<T> m1, T dt)
    {
        glm::tmat3x3<T> result;
        for (int i = 0; i < 3; i++)
        {
            result[i] = glm::lerp(m0[i], m1[i], dt);
        }
        return result;
    }

    template<typename T>
    glm::tmat4x4<T> abs(glm::tmat4x4<T> m)
    {
        glm::tmat4x4<T> result;
        for (int i = 0; i < 4; i++)
        {
            result[i] = glm::abs(m[i]);
        }
        return result;
    }

    template<typename T>
    glm::tmat3x3<T> abs(glm::tmat3x3<T> m)
    {
        glm::tmat3x3<T> result;
        for (int i = 0; i < 3; i++)
        {
            result[i] = glm::abs(m[i]);
        }
        return result;
    }

    template<typename T>
    T min(T v)
    {
        return v;
    }

    template<typename T, typename... Tr>
    T min(T v0, Tr... v)
    {
        return std::min(v0, min(v...));
    }

    template<typename T>
    T max(T v)
    {
        return v;
    }

    template<typename T, typename... Tr>
    T max(T v0, Tr... v)
    {
        return std::max(v0, max(v...));
    }

	// TODO: figure out this bit here
	bool quadratic(EFloat a, EFloat b, EFloat c, EFloat* t0, EFloat* t1)
	{
		return true;
	}

	// can't find swizzle for some reason:
	template<typename T>
	glm::tvec2<T> permute(glm::tvec2<T> v, int x, int y)
	{
		glm::tvec3<T> res;
		res[x] = v.x;
		res[y] = v.y;
		return res;
	}

	template<typename T>
	glm::tvec3<T> permute(glm::tvec3<T> v, int x, int y, int z)
	{
		glm::tvec3<T> res;
		res[x] = v.x;
		res[y] = v.y;
		res[z] = v.z;
		return res;
	}

	template<typename T>
	glm::tvec4<T> permute(glm::tvec4<T> v, int x, int y, int z, int w)
	{
		glm::tvec3<T> res;
		res[x] = v.x;
		res[y] = v.y;
		res[z] = v.z;
		res[w] = v.w;
		return res;
	}

	template<typename T>
	int maxDimension(glm::tvec3<T> v)
	{
		if (v.x > v.y && v.x > v.z)
		{
			return 0;
		}

		if (v.y > v.z)
		{
			return 1;
		}

		return 2;
	}

	template<typename T>
	void coordinateSystem(glm::tvec3<T> v1, glm::tvec3<T>* v2, glm::tvec3<T>* v2)
	{
		if (std::abs(v1.x) > std::abs(v1.y))
		{
			*v2 = glm::tvec3<T>(-v1.z, 0, v1.x) / std::sqrt(v1.x * v1.x, )
		}
	}
}