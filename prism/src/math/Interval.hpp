#pragma once

#include "math.hpp"

#include <algorithm>

#include "Utility.hpp"

namespace prism
{
    template<class T>
    class tInterval
    {
    public:
		tInterval() noexcept
        {
        }

		tInterval(T t) :
            low(t),
            high(t)
        {
        }

		tInterval(T low, T high) :
            low(std::min(low, high)),
            high(std::max(low, high))
        {
        }

		tInterval<T> operator+(tInterval<T> i) const
        {
            return Interval<T>(low + i.low, high + i.high);
        }

		tInterval<T> operator-(tInterval<T> i) const
        {
            return Interval<T>(low - i.low, high - i.high);
        }

		tInterval<T> operator*(tInterval<T> i) const
        {
            const T s0 = low * i.low;
            const T s1 = low * i.high;
            const T s2 = high * i.low;
            const T s3 = high * i.high;

            return tInterval<T>(min(s0, s1, s2, s3), max(s0, s1, s2, s3));
        }

		tInterval<T> operator/(tInterval<T> i) const
        {
            const T s0 = low / i.low;
            const T s1 = low / i.high;
            const T s2 = high / i.low;
            const T s3 = high / i.high;

            return tInterval<T>(min(s0, s1, s2, s3), max(s0, s1, s2, s3));
        }

		tInterval<T> operator-() const
        {
            return Interval<T>(-high, -low);
        }

		tInterval<T>& operator+=(tInterval<T> i)
        {
            *this = *this + i;
            return *this;
        }

		tInterval<T>& operator-=(tInterval<T> i)
        {
            *this = *this - i;
            return *this;
        }

		tInterval<T>* operator*=(tInterval<T> i)
        {
            *this = *this * i;
            return *this;
        }

    public:
        T low;
        T high;
    };

	using Interval = tInterval<Float>;

	template<class T>
	tInterval<T> cos(tInterval<T> i)
	{
		T cosLow = std::cos(i.low);
		T cosHigh = std::cos(i.high);

		if (cosLow > cosHigh)
		{
			std::swap(cosLow, cosHigh);
		}

		if (i.low < pi<T>() && i.high > pi<T>())
		{
			cosLow = -1;
		}

		return tInterval<T>(cosLow, cosHigh);
	}

    template<class T>
	tInterval<T> sin(tInterval<T> i)
    {
        T sinLow = std::sin(i.low);
        T sinHigh = std::sin(i.high);

        if (sinLow > sinHigh)
        {
            std::swap(sinLow, sinHigh);
        }

        if (i.low < hlfPI<T>() && i.high > hlfPI<T>())
        {
            sinHigh = 1;
        }

        if (i.low < ((3. / 2.) * pi<T>()) && i.high > ((3. / 2.) * pi<T>()))
        {
            sinLow = -1;
        }

        return tInterval<T>(sinLow, sinHigh);
    }
}