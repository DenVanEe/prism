#pragma once

#include "math.hpp"

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/norm.hpp>

#include "Ray.hpp"
#include "Bound.hpp"
#include "Interaction.hpp"
#include "Floating.hpp"

namespace prism
{
    class Transform
    {
    public:
        Transform() :
            m_trans(1),
            m_invTrans(1)
        {
        }

        Transform(Mat4 trans) :
            m_trans(trans),
            m_invTrans(glm::inverse(trans))
        {
        }

        Transform(Mat4 trans, Mat4 invTrans) :
            m_trans(trans),
            m_invTrans(trans)
        {
        }

        Vec3 pnt(Vec3 p) const
        {
            const Vec4 pw(p, 1);
            const Vec4 res = m_trans * pw;
            return res.w == 1 ? Vec3(res) : Vec3(res / res.w);
        }

		Vec3 pnt(Vec3 p, Vec3* pErr) const
		{
			const Vec4 pw(p, 1);
			const Vec4 res = m_trans * pw;
			*pErr = gamma<Float>(3) * glm::abs(Vec3(res));
			return res.w == 1 ? Vec3(res) : Vec3(res / res.w);
		}

		Vec3 pnt(Vec3 p, Vec3 ptErr, Vec3* pErr) const
		{
			const Vec4 pw(p, 1);
			const Vec4 pwErr(ptErr, 1);

			const Vec4 res = m_trans * pw;
			const Vec4 res1 = m_trans * pwErr;

			*pErr = gamma<Float>(3) * glm::abs(Vec3(res)) + (gamma<Float>(3) + 1) * glm::abs(Vec3(res1));
			return res.w == 1 ? Vec3(res) : Vec3(res / res.w);
		}

		//226

        Vec3 nrm(Vec3 n) const
        {
            const Mat4 trans = glm::transpose(m_invTrans);
            const Vec4 nw(n, 0);
            const Vec4 res = trans * nw;
            return Vec3(res);
        }

        Vec3 vec(Vec3 v) const
        {
            const Vec4 vw(v, 0);
            const Vec4 res = m_trans * vw;
            return Vec3(res);
        }

		Vec3 vec(Vec3 v, Vec3* vErr) const
		{
			const Vec4 pw(v, 0);
			const Vec4 res = m_trans * pw;
			*vErr = gamma<Float>(3) * glm::abs(Vec3(res));
			return res.w == 1 ? Vec3(res) : Vec3(res / res.w);
		}

		Vec3 vec(Vec3 v, Vec3 vtErr, Vec3* vErr) const
		{
			const Vec4 pw(v, 0);
			const Vec4 pwErr(vtErr, 0);

			const Vec4 res = m_trans * pw;
			const Vec4 res1 = m_trans * pwErr;

			*vErr = gamma<Float>(3) * glm::abs(Vec3(res)) + (gamma<Float>(3) + 1) * glm::abs(Vec3(res1));
			return res.w == 1 ? Vec3(res) : Vec3(res / res.w);
		}

        Ray ray(Ray r) const
        {
			Vec3 oErr;
            Vec3 org = pnt(r.org, &oErr);
            const Vec3 dir = vec(r.dir);
			const Float len2 = glm::length2(dir);
			Float maxTime = r.tMax;
			if (len2 > 0)
			{
				const Float dt = glm::dot(abs(dir), oErr) / len2;
				org += dir * dt;
				maxTime -= dt;
			}
            return Ray(org, dir, r.time, maxTime, r.med);
        }

		Ray ray(Ray r, Vec3* oErr, Vec3* dErr) const
		{
			Vec3 org = pnt(r.org, oErr);
			const Vec3 dir = vec(r.dir);
			const Float len2 = glm::length2(dir);
			Float maxTime = r.tMax;
			if (len2 > 0)
			{
				const Float dt = glm::dot(abs(dir), *oErr) / len2;
				org += dir * dt;
				// maxTime -= dt;
				// this was commented out, not sure why, so yeah.
			}
			return Ray(org, dir, r.time, maxTime, r.med);
		}

        Bound3 bnd(Bound3 box) const
        {
            // Based on Arvo 1990 (Graphics Gems 1)

            Vec3 pmin = Vec3(m_trans[3]);
            Vec3 pmax = Vec3(m_trans[3]);

            Mat3 rot = Mat3(m_trans);

            for (int i = 0; i < 3; i++)
            {
                Vec3 a = rot[i] * box.pmin;
                Vec3 b = rot[i] * box.pmax;
                Vec3 min = glm::min(a, b);
                Vec3 max = glm::max(a, b);
                pmin += min;
                pmax += max;
            }

            return Bound3(pmin, pmax);
        }

		SurfaceInteraction snt(const SurfaceInteraction& si) const
		{
			Vec3 err;
			const Vec3 p = pnt(si.p, si.err, &err);

			const Vec3 n = glm::normalize(nrm(si.n));
			const Vec3 wo = glm::normalize(vec(si.wo));
			const Float time = si.time;
			const MediumInterface mediumInterface = si.mediumInterface;
			const Vec2 uv = si.uv;
			const Shape* shape = si.shape;
			const Vec3 dpdu = vec(si.dpdu);
			const Vec3 dpdv = vec(si.dpdv);
			const Vec3 dndu = nrm(si.dndu);
			const Vec3 dndv = nrm(si.dndv);
			SurfaceInteraction::SH shading;
			shading.n = glm::normalize(nrm(si.shading.n));
			shading.dpdu = vec(si.shading.dpdu);
			shading.dpdv = vec(si.shading.dpdv);
			shading.dndu = nrm(si.shading.dndu);
			shading.dndv = nrm(si.shading.dndv);
			// TODO: Add the other crap we don't care about right now
			SurfaceInteraction res = SurfaceInteraction(p, err, uv, wo, dpdu, dpdv, dndu, dndv, time, shape);
			res.setShadingGeometry(shading, true);
			res.mediumInterface = mediumInterface;
			return res;
		}

		// 118

        bool hasScale() const
        {
            const Float lenX = glm::length2(this->vec(Vec3(1, 0, 0)));
            const Float lenY = glm::length2(this->vec(Vec3(0, 1, 0)));
            const Float lenZ = glm::length2(this->vec(Vec3(0, 0, 1)));

            const bool notOneX = !cmpConstEps(lenX, 1, 0.001);
            const bool notOneY = !cmpConstEps(lenY, 1, 0.001);
            const bool notOneZ = !cmpConstEps(lenZ, 1, 0.001);

            return notOneX || notOneY || notOneZ;
        }

        Transform operator*(const Transform& t) const
        {
            Mat4 trans = m_trans * t.m_trans;
            Mat4 invTrans = t.m_invTrans * m_invTrans;
            return Transform(trans, invTrans);
        }

        bool swapsHandedness() const
        {
            Mat3 mat = Mat3(m_trans);
            return glm::determinant(mat) < 0;
        }

        bool operator==(const Transform& t) const
        {
            return m_trans == t.m_trans;
        }

        bool operator!=(const Transform& t) const
        {
            return m_trans != t.m_trans;
        }

        Mat4 getMatrix() const
        {
            return m_trans;
        }

        Mat4 getInvMatrix() const
        {
            return m_invTrans;
        }

    public:
        friend Transform inverse(const Transform& trans)
        {
            return Transform(trans.m_invTrans, trans.m_trans);
        }

        friend Transform transpose(const Transform& trans)
        {
            return Transform(glm::transpose(trans.m_trans), glm::transpose(trans.m_invTrans));
        }

		friend bool isIdentity(const Transform& trans)
		{
			return trans.m_trans == Mat4(F(1));
		}

    private:
        Mat4 m_trans;
        Mat4 m_invTrans;
    };

    Transform translate(Vec3 delta)
    {
        const Mat4 trans = glm::translate(delta);
        const Mat4 invTrans = glm::translate(-delta);
        return Transform(trans, invTrans);
    }

    Transform scale(Vec3 scale)
    {
        const Vec3 invScale = Vec3(1) / scale;
        const Mat4 trans = glm::scale(scale);
        const Mat4 invTrans = glm::scale(invScale);
        return Transform(trans, invTrans);
    }

    // glm doesn't have a seperate rotateX, rotateY, and rotateZ function that
    // returns the matrix itself. Though looking at the source code, it
    // seems all transformations get formed by multiplying the identity anyways.
    // I don't see this being used often enough (like in a game) where this could
    // lead to a noticible performance difference.

    Transform rotate(Float degrees, Vec3 axis)
    {
        const Float angle = glm::radians(degrees);
        const Mat4 trans = glm::rotate(angle, axis);
        const Mat4 invTrans = glm::rotate(angle, -axis);
        return Transform(trans, invTrans);
    }

    Transform lookAt(Vec3 pos, Vec3 look, Vec3 up)
    {
        const Mat4 trans = glm::lookAt(pos, look, up);
        return Transform(trans, glm::inverse(trans));
    }
}