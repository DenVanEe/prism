#pragma once

#define GLM_FORCE_INLINE
#define GLM_FORCE_AVX
#define GLM_FORCE_LEFT_HANDED
#define GLM_FORCE_EXPLICIT_CTOR
#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtc/constants.hpp>
#include <limits>
#include <glm/gtx/quaternion.hpp>
#include <glm/fwd.hpp>

namespace prism
{
    using Float = float; // double;
    
    using Vec2 = glm::tvec2<Float>;
    using Vec3 = glm::tvec3<Float>;
    using Vec4 = glm::tvec4<Float>;

    using Quat = glm::tquat<Float>;

    using Mat3 = glm::tmat3x3<Float>;
    using Mat4 = glm::tmat4x4<Float>;

    template<typename T>
    constexpr Float F(T f)
    {
        return static_cast<Float>(f);
    }

	template<typename T>
	constexpr float flt(T f)
	{
		return static_cast<float>(f);
	}

	template<typename T>
	constexpr double dbl(T f)
	{
		return static_cast<double>(f);
	}

    template<class T>
    constexpr T inf()
    {
        static_assert(std::numeric_limits<T>::has_infinity, "Type does not support infinity.");
        return std::numeric_limits<T>::infinity();
    }

    template<class T>
    constexpr T nan()
    {
        static_assert(std::numeric_limits<T>::has_quiet_NaN, "Type does not support NaN.");
        return std::numeric_limits<T>::quiet_NaN();
    }

    template<class T>
    constexpr T max()
    {
        return std::numeric_limits<T>::max();
    }

    template<class T>
    constexpr T min()
    {
        return std::numeric_limits<T>::min();
    }

    template<class T>
    constexpr T machEpsilon()
    {
        return std::numeric_limits<T>::epsilon() * 0.5;
    }

    // Just because these are so popular:
    template<class T>
    constexpr T pi()
    {
        return glm::pi<T>();
    }

    template<class T>
    constexpr T hlfPI()
    {
        return pi<T>() / 2;
    }

    template<class T>
    constexpr T dblPI()
    {
        return pi<T>() * 2;
    }

    // If you know exactly what you are doing and whatnot, that is important here, most important.
    // IMPORTANT: it will cast the second argument and eps to match the type that is the FIRST argument.
    template<class T, class T0, class T1>
    constexpr bool cmpConstEps(T val, T0 cmp, T1 eps)
    {
        static_assert(std::is_arithmetic_v<T>);

        const T castCmp = static_cast<T>(cmp);
        const T castEps = static_cast<T1>(eps);
        if constexpr (std::is_integral_v<T>)
        {
            return val == castCmp;
        }
        else
        {
            const T absDiff = std::abs(val - castCmp);
            return absDiff <= castEps;
        }
    }
}