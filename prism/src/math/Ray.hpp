#pragma once

#include "math.hpp"

#include <glm/vec3.hpp>
#include <glm/gtx/compatibility.hpp>

namespace prism
{
    class Medium;

    template<typename T>
    class tRay
    {
    public:
        tRay() :
            time(0),
            tMax(inf<T>()),
            med(nullptr)
        {
        }

        tRay(glm::tvec3<T> org, glm::tvec3<T> dir, T time, T maxTime, const Medium* med) :
            time(time),
            tMax(maxTime),
            med(med),
            org(org),
            dir(dir)
        {
        }

        tRay(glm::tvec3<T> org, glm::tvec3<T> dir, tRay<T> ray) :
            time(ray.time),
            tMax(ray.tMax),
            med(ray.med),
            org(org),
            dir(dir)
        {
        }

        glm::tvec3<T> operator()(T t) const
        {
            return org + dir * t;
        }

    public:
        T time;
        mutable T tMax;

        const Medium* med;

        glm::tvec3<T> org;
        glm::tvec3<T> dir;
    };

    using Ray  = tRay<Float>;
	using ERay = tRay<EFloat>;

    template<class T>
    class tRayDiff : public tRay<T>
    {
    public:
        tRayDiff() :
            tRay<T>(),
            hasDiff(false)
        {
        }

        tRayDiff(glm::tvec3<T> org, glm::tvec3<T> dir, T time, T maxTime, const Medium* med) :
            tRay<T>(org, dir, time, maxTime, med),
            hasDiff(false)
        {
        }

        tRayDiff(tRay<T> r) :
            tRay<T>(r),
            hasDiff(false)
        {
        }

        void scaleDiff(float s)
        {
            rxorg = glm::lerp(org, rxorg, s);
            ryorg = glm::lerp(org, ryorg, s);
            rxdir = glm::lerp(dir, rxdir, s);
            rydir = glm::lerp(dir, rydir, s);
        }

    public:
        bool hasDiff;
        glm::tvec3<T> rxorg, ryorg;
        glm::tvec3<T> rxdir, rydir;
    };

    using RayDiff = tRayDiff<Float>;
}