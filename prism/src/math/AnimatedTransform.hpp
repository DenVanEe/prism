#pragma once

#include "math.hpp"

#include <glm/vec3.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/mat4x4.hpp>

#include "Transform.hpp"
#include "Utility.hpp"
#include "Bound.hpp"

namespace prism
{
    class AnimatedTransform
    {
    public:
        AnimatedTransform(const Transform* startTrans, Float startTime, const Transform* endTrans, Float endTime) :
            m_startTrans(startTrans),
            m_endTrans(endTrans),
            m_startTime(startTime),
            m_endTime(endTime),
            m_animates(*startTrans != *endTrans)
        {
            decompose(m_startTrans->getMatrix(), &m_trans[0], &m_rot[0], &m_scale[0]);
            decompose(m_endTrans->getMatrix(), &m_trans[1], &m_rot[1], &m_scale[1]);
            if (glm::dot(m_rot[0], m_rot[1]) < 0)
            {
                m_rot[1] = -m_rot[1];
            }
            m_rotates = glm::dot(m_rot[0], m_rot[1]) < 0.9995;
			if (m_rotates)
			{
				genDerivativeTerms();
			}
        }

        Transform interpolate(Float time) const
        {
            if (!m_animates || time <= m_startTime)
            {
                return *m_startTrans;
            }
            else if (time >= m_endTime)
            {
                return *m_endTrans;
            }

            return interpolateNoCheck(time);
        }

        Vec3 pnt(Float time, Vec3 p) const
        {
            if (!m_animates || time <= m_startTime)
            {
                return m_startTrans->pnt(p);
            }
            else if (time >= m_endTime)
            {
                return m_endTrans->pnt(p);
            }

            Transform trans = interpolateNoCheck(time);
            return trans.pnt(p);
        }

        Vec3 nrm(Float time, Vec3 n) const
        {
            if (!m_animates || time <= m_startTime)
            {
                return m_startTrans->nrm(n);
            }
            else if (time >= m_endTime)
            {
                return m_endTrans->nrm(n);
            }

            Transform trans = interpolateNoCheck(time);
            return trans.nrm(n);
        }

        Vec3 vec(Float time, Vec3 v) const
        {
            if (!m_animates || time <= m_startTime)
            {
                return m_startTrans->vec(v);
            }
            else if (time >= m_endTime)
            {
                return m_endTrans->vec(v);
            }

            Transform trans = interpolateNoCheck(time);
            return trans.vec(v);
        }

        Ray ray(Ray r) const
        {
            if (!m_animates || r.time <= m_startTime)
            {
                return m_startTrans->ray(r);
            }
            else if (r.time >= m_endTime)
            {
                return m_endTrans->ray(r);
            }

            Transform trans = interpolateNoCheck(r.time);
            return trans.ray(r);
        }

        Bound3 boundMotion(Bound3 b) const
        {
            if (!m_animates)
            {
                return m_startTrans->bnd(b);
            }
            else if (!m_rotates)
            {
                return combine(m_startTrans->bnd(b), m_endTrans->bnd(b));
            }

            Bound3 result;
            for (unsigned i = 0; i < 8; i++)
            {
                result = combine(result, boundPointMotion(b.corner(i)));
            }
            return result;
        }

    private:
		class DerivativeTerm
		{
		public:
			DerivativeTerm()
			{
			}

			DerivativeTerm(Float c, Float x, Float y, Float z) :
				m_kc(c),
				m_kv(Vec3(x, y, z))
			{
			}

			DerivativeTerm(Float c, Vec3 v) :
				m_kc(c),
				m_kv(v)
			{
			}

			Float eval(Vec3 p) const
			{
				return glm::dot(m_kv, p) + m_kc;
			}

		private:
			Float m_kc;
			Vec3  m_kv;
		};

		DerivativeTerm c1[3], c2[3], c3[3], c4[3], c5[3];

		void genDerivativeTerms();

        Bound3 boundPointMotion(Vec3 p) const;

        // I could use glm's decompose function, but I feel more comfortable
        // using the book's implementation.
        void decompose(Mat4 m, Vec3* t, Quat* r, Mat4* s) const
        {
            *t = Vec3(m[3]);
            const Mat3 rotscale = Mat3(m);

            // polar decomposition. I should take more lin. algb....

            Float norm;
            unsigned count = 0;
            Mat4 rot = m;
            do
            {
                const Mat4 rotIt = glm::inverse(glm::transpose(rot));
                const Mat4 rotNext = F(0.5) * (rot + rotIt);
                const Mat4 diff = abs(rot - rotNext);
                // sum up diff:
                Vec4 sumDiff = Vec4(0);
                for (unsigned i = 0; i < 4; i++)
                {
                    sumDiff += diff[i];
                }
                norm = sumDiff.x + sumDiff.y + sumDiff.z; // don't care about w component
                rot = rotNext;
            } while (++count < 100 && norm > 0.0001);
            *r = glm::toQuat(rot);
            *s = glm::inverse(rot) * m;
        }

        Transform interpolateNoCheck(Float time) const
        {
            const Float dt = (time - m_startTime) / (m_endTime - m_startTime);
            const Vec3 trans = glm::lerp(m_trans[0], m_trans[1], dt);
            const Quat rot = glm::slerp(m_rot[0], m_rot[1], dt);
            const Mat4 scale = lerp(m_scale[0], m_scale[1], dt);
            return translate(trans) * Transform(glm::toMat4(rot)) * Transform(scale);
        }

    private:
        const Transform* m_startTrans;
        const Transform* m_endTrans;
        const Float m_startTime;
        const Float m_endTime;
        const bool m_animates;

        Vec3 m_trans[2];
        Quat m_rot[2];
        Mat4 m_scale[2];
        bool m_rotates;
    };
}