#pragma once

#include "math.hpp"

#include <glm/vec3.hpp>
#include <glm/glm.hpp>

namespace prism
{
    template<class T>
    class tBoundSphere
    {
    public:
        tBoundSphere()
        {
        }

        tBoundSphere(glm::tvec3<T> org, T rad) :
            rad(rad),
            org(org)
        {
        }

    public:
        T             rad;
        glm::tvec3<T> org;
    };

    using BoundSphere = tBoundSphere<Float>;

    template<class T>
    class tBound3
    {
    public:
        tBound3() :
            pmin(max<T>()),
            pmax(min<T>())
        {
        }

        tBound3(glm::tvec3<T> p) :
            pmin(p),
            pmax(p)
        {
        }

        tBound3(glm::tvec3<T> p0, glm::tvec3<T> p1) :
            pmin(glm::min(p0, p1)),
            pmax(glm::max(p0, p1))
        {
        }

        glm::tvec3<T>& operator[](unsigned i)
        {
            switch (i)
            {
            case 0:
                return pmin;
            case 1:
                return pmax;
            default:
                assert(false);
            }
        }

        glm::tvec3<T> operator[](unsigned i) const
        {
            switch (i)
            {
            case 0:
                return pmin;
            case 1:
                return pmax;
            default:
                assert(false);
            }
        }

        glm::tvec3<T> corner(unsigned i) const
        {
            const T x = (*this)[(corner & 1)].x;
            const T y = (*this)[(corner & 2) ? 1 : 0].y;
            const T z = (*this)[(corner & 4) ? 1 : 0].z;
            return glm::tvec3<T>(x, y, z);
        }

		bool intersectP(Ray ray, Float *htt0, Float *htt1) const
		{
			Float t0 = 0;
			Float t1 = ray.maxTime;
			for (unsigned i = 0; i < 3; i++)
			{
				const Float invDir = 1 / ray.dir[i];
				Float tNear = (pmin[i] - ray.org[i]) * invDir;
				Float tFar = (pmax[i] - ray.org[i]) * invDir;
				if (tNear > tFar)
				{
					std::swap(tNear, tFar);
				}

				t0 = tNear > t0 ? tNear : t0;
				t1 = tFar < t1 ? tFar : t1;

				if (t0 > t1)
				{
					return false;
				}
			}
			if (htt0)
			{
				*htt0 = t0;
			}
			if (htt1)
			{
				*htt1 = t1;
			}
			return true;
		}

		bool intersectP(Ray ray, Vec3 invDir, const bool isDirNeg[3]) const
		{
			const Bound3& bound = *this;
			Float tMin =  (bound[    isDirNeg[0]].x - ray.org.x) * invDir.x;
			Float tMax =  (bound[1 - isDirNeg[0]].x - ray.org.x) * invDir.x;
			Float tyMin = (bound[    isDirNeg[1]].y - ray.org.y) * invDir.y;
			Float tyMax = (bound[1 - isDirNeg[1]].y - ray.org.y) * invDir.y;
			
			tMax *= 1 + 2 * gamma<Float>(3);
			tyMax *= 1 + 2 * gamma<Float>(3);

			if (tMin > tyMax || tyMin > tMax)
			{
				return false;
			}
			if (tyMin > tMin)
			{
				tMin = tyMin;
			}
			if (tyMax < tMax)
			{
				tMax = tyMax;
			}

			Float tzMin = (bound[    isDirNeg[2]].z - ray.org.z) * invDir.z;
			Float tzMax = (bound[1 - isDirNeg[2]].z - ray.org.z) * invDir.z;

			tzMax *= 1 + 2 * gamma<Float>(3);
			if (tMin > tzMax || tzMin > tMax)
			{
				return false;
			}
			if (tzMin > tMin)
			{
				tMin = tzMin;
			}
			if (tzMax < tMax)
			{
				tMax = tzMax;
			}

			return (tMin < ray.maxTime) && (tMax > 0);
		}

    public:
        glm::tvec3<T> pmin, pmax;
    };

    using Bound3 = tBound3<Float>;

    template<class T>
    tBound3<T> combine(tBound3<T> b, glm::tvec3<T> p)
    {
        const glm::tvec3<T> pmin = glm::min(b.pmin, p);
        const glm::tvec3<T> pmax = glm::max(b.pmax, p);
        return Bound3<T>(pmin, pmax);
    }

    template<class T>
    tBound3<T> combine(tBound3<T> b0, tBound3<T> b1)
    {
        const glm::tvec3<T> pmin = glm::min(b0.pmin, b1.pmin);
        const glm::tvec3<T> pmax = glm::max(b0.pmax, b1.pmax);
        return Bound3<T>(pmin, pmax);
    }

    template<class T>
    tBound3<T> intersect(tBound3<T> b0, tBound3<T> b1)
    {
        const glm::tvec3<T> pmin = glm::max(b0.pmin, b1.pmin);
        const glm::tvec3<T> pmax = glm::min(b0.pmax, b1.pmax);
        return Bound3<T>(pmin, pmax);
    }

    template<class T>
    bool overlaps(tBound3<T> b0, tBound3<T> b1)
    {
        const bool x = b0.pmin.x >= b1.pmin.x && b0.pmin.x <= b1.pmax.x;
        const bool y = b0.pmin.y >= b1.pmin.y && b0.pmin.y <= b1.pmax.y;
        const bool z = b0.pmin.z >= b1.pmin.z && b0.pmin.z <= b1.pmax.z;
        return x && y && z;
    }

    template<class T>
    bool inside(tBound3<T> b, glm::tvec3<T> p)
    {
        const bool x = p.x >= b.pmin.x && p.x <= b.pmax.x;
        const bool y = p.y >= b.pmin.y && p.y <= b.pmax.y;
        const bool z = p.z >= b.pmin.z && p.z <= b.pmax.z;
        return x && y && z;
    }

    template<class T>
    tBound3<T> expand(tBound3<T> b, T d)
    {
        const glm::tvec3<T> delta(d);
        const glm::tvec3<T> pmin = b.pmin - delta;
        const glm::tvec3<T> pmax = b.pmax + delta;
        return Bound3<T>(pmin, pmax);
    }

    template<class T>
    glm::tvec3<T> diagonal(tBound3<T> b)
    {
        return b.pmax - b.pmin;
    }

    template<class T>
    glm::tvec3<T> surfaceArea(tBound3<T> b)
    {
        const glm::tvec3<T> d = diagonal(b);
        return 2 * (d.x * d.y + d.x * d.y + d.y * d.z);
    }

    template<class T>
    glm::tvec3<T> volume(tBound3<T> b)
    {
        const glm::tvec3<T> d = diagonal(b);
        return d.x * d.y * d.z;
    }

    template<class T>
    unsigned maxAxis(tBound3<T> b)
    {
        const glm::tvec3<T> d = diagonal(b);
        if (d.x > d.y && d.x > d.z)
        {
            return 0;
        }
        else if (d.y > d.z)
        {
            return 1;
        }
        return 2;
    }

    template<class T>
    glm::tvec3<T> lerp(tBound3<T> b, glm::tvec3<T> t)
    {
        return glm::lerp(b.pmin, b.pmax, t);
    }

    template<class T>
    glm::tvec3<T> offset(tBound3<T> b, glm::tvec3<T> p)
    {
        glm::tvec3<T> o = p - b.pmin;
        if (b.pmax.x > b.pmin.x)
        {
            o.x /= b.pmax.x - b.pmin.x;
        }
        if (b.pmax.y > b.pmin.y)
        {
            o.y /= b.pmax.y - b.pmin.y;
        }
        if (b.pmax.z > b.pmin.z)
        {
            o.z /= b.pmax.z - b.pmin.z;
        }
        return o;
    }

    template<class T>
    tBoundSphere<T> boundingSphere(tBound3<T> b, glm::tvec3<T> p)
    {
        const glm::tvec3<T> org = (b.pmin + b.pmax) / 2;
        const T rad = inside(b, org) ? glm::length(b.pmax - org) : 0;
        return BoundSphere<T>(org, rad);
    }

    // pg 81
}