#pragma once

#include "math.hpp"

#include "MediumInterface.hpp"
#include "Utility.hpp"
#include "../shapes/Shape.hpp"

namespace prism
{
	// pg 117 - TODO: figure out if I should make the members public or not

	class Interaction
	{
	public:
		Interaction()
		{
		}

		// wo or omega_o (outgoing direction for light and whatnot)
		Interaction(Vec3 p, Vec3 n, Vec3 err, Vec3 wo, Float time) :
			p(p),
			n(n),
			wo(wo),
			time(time)
		{
		}

		bool isSurfaceInteraction() const
		{
			return n != Vec3(0);
		}

	public:
		Vec3 p, n, err, wo;
		Float time;
		MediumInterface mediumInterface;
	};

	class Primitive;
	class BSDF;
	class BSSRDF;

	class SurfaceInteraction : public Interaction
	{
	public:
		// For use when bump-mapping or something, where the actual geometry
		// differs.
		struct SH
		{
			Vec3 n;
			Vec3 dpdu, dpdv;
			Vec3 dndu, dndv;

			SH()
			{
			}

			SH(Vec3 dpdu, Vec3 dpdv, Vec3 dndu, Vec3 dndv) :
				dpdu(dpdu), dpdv(dpdv), dndu(dndu), dndv(dndv)
			{
			}

			SH(Vec3 n, Vec3 dpdu, Vec3 dpdv, Vec3 dndu, Vec3 dndv) :
				n(n), dpdu(dpdu), dpdv(dpdv), dndu(dndu), dndv(dndv)
			{
			}
		};

	public:
		SurfaceInteraction() :
			primitive(nullptr),
			bsdf(nullptr),
			bssrdf(nullptr)
		{
		}

		SurfaceInteraction(Vec3 p, Vec3 err, Vec2 uv, Vec3 wo,
			Vec3 dpdu, Vec3 dpdv, Vec3 dndu, Vec3 dndv,
			Float time, const Shape* shape) :
			Interaction(p, glm::normalize(glm::cross(dpdu, dpdv)), err, wo, time),
			uv(uv),
			dpdu(dpdu),
			dpdv(dpdv),
			dndu(dndu),
			dndv(dndv),
			shape(shape),
			primitive(nullptr),
			bsdf(nullptr),
			bssrdf(nullptr),
			shading(n, dpdu, dpdv, dndu, dndv)
		{
			// swap handedness if the following holds:
			if (shape && (shape->reverseOrientation ^ shape->transSwapsHandedness))
			{
				n = -n;
				shading.n = -shading.n;
			}
		}

		void setShadingGeometry(SH shad, bool shadingIsAuthoritative)
		{
			shading = shading;
			shading.n = glm::normalize(glm::cross(shading.dpdu, shading.dpdv));
			if (shape && (shape->reverseOrientation ^ shape->transSwapsHandedness))
			{
				shading.n = -shading.n;
			}
			if (shadingIsAuthoritative)
			{
				n = alignDirection(shading.n, n);
			}
			else
			{
				shading.n = alignDirection(n, shading.n);
			}
		}

	public:
		Vec2 uv;
		Vec3 dpdu, dpdv;
		Vec3 dndu, dndv;

		const Shape*     shape;
		const Primitive* primitive;

		BSDF*   bsdf;
		BSSRDF* bssrdf;

		SH shading;
	};
}