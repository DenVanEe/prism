#pragma once

#include "math.hpp"

#include <cstdint>
#include <cstring>

#include "Interval.hpp"

namespace prism
{
	static_assert(sizeof(float) == sizeof(std::uint32_t));
	static_assert(sizeof(double) == sizeof(std::uint64_t));

    std::uint32_t floatToBits(float f)
    {
        std::uint32_t loc;
        std::memcpy(&loc, &f, sizeof(float));
        return loc;
    }

    float bitsToFloat(std::uint32_t f)
    {
        float loc;
        std::memcpy(&loc, &f, sizeof(float));
        return loc;
    }

    std::uint64_t floatToBits(double f)
    {
        std::uint64_t loc;
        std::memcpy(&loc, &f, sizeof(double));
        return loc;
    }

    double bitsToFloat(std::uint64_t f)
    {
        double loc;
        std::memcpy(&loc, &f, sizeof(double));
        return loc;
    }

    float nextFloatUp(float f)
    {
        if (std::isinf(f) && f > 0.f)
        {
            return f;
        }
        if (f == -0.f)
        {
            return 0.f;
        }

        const std::uint32_t b = floatToBits(f);
        const std::uint32_t ib = f >= 0.f ? b + 1 : b - 1;
        return bitsToFloat(ib);
    }

    double nextFloatUp(double f)
    {
        if (std::isinf(f) && f > 0.f)
        {
            return f;
        }
        if (f == -0.f)
        {
            return 0.f;
        }

        const std::uint64_t b = floatToBits(f);
        const std::uint64_t ib = f >= 0.f ? b + 1 : b - 1;
        return bitsToFloat(ib);
    }

    float nextFloatDown(float f)
    {
        if (std::isinf(f) && f < 0.f)
        {
            return f;
        }
        if (f == 0.f)
        {
            return -0.f;
        }

        const std::uint32_t b = floatToBits(f);
        const std::uint32_t ib = f >= 0.f ? b - 1 : b + 1;
        return bitsToFloat(ib);
    }

    double nextFloatDown(double f)
    {
        if (std::isinf(f) && f < 0.f)
        {
            return f;
        }
        if (f == 0.f)
        {
            return -0.f;
        }

        const std::uint64_t b = floatToBits(f);
        const std::uint64_t ib = f >= 0.f ? b - 1 : b + 1;
        return bitsToFloat(ib);
    }

    template<typename T>
    Interval<T> nextFloatInterval(Interval<T> i)
    {
        static_assert(std::is_floating_point_v<T>, "Next float interval only works for doubles and floats.");
        const T low = nextFloatDown(i.low);
        const T high = nextFloatUp(i.high);
        return tInterval<T>(low, high);
    }

    template<typename T>
    constexpr T gamma(int n)
    {
        return (n * machEpsilon<T>()) / (1 - n * machEpsilon<T>());
    }

    template<typename T>
    class tEFloat
    {
        static_assert(std::is_floating_point_v<T>, "EFloat only works for floating point types.");

    public:
		tEFloat() noexcept
        {
        }

		tEFloat(T val, T err = 0) :
            m_val(val),
            m_err(err)
        {
#ifndef NDEBUG
            m_perc = m_val;
#endif
        }

        explicit operator float() const
        {
            return static_cast<float>(m_val);
        }

        explicit operator double() const
        {
            return static_cast<double>(m_val);
        }

        explicit operator long double() const
        {
            return static_cast<long double>(m_val);
        }

		tEFloat<T> operator+(tEFloat<T> f) const
        {
			tEFloat<T> result;
            result.m_val = m_val + f.m_val;
#ifndef NDEBUG
            result.m_perc = m_perc + f.m_perc;
#endif
            result.m_err = nextFloatInterval(m_err + f.m_err);
            return result;
        }

		tEFloat<T> operator-(tEFloat<T> f) const
        {
			tEFloat<T> result;
            result.m_val = m_val - f.m_val;
#ifndef NDEBUG
            result.m_perc = m_perc - f.m_perc;
#endif
            result.m_err = nextFloatInterval(m_err - f.m_err);
            return result;
        }

		tEFloat<T> operator*(tEFloat<T> f) const
        {
			tEFloat<T> result;
            result.m_val = m_val * f.m_val;
#ifndef NDEBUG
            result.m_perc = m_perc * f.m_perc;
#endif
            result.m_err = nextFloatInterval(m_err * f.m_err);
            return result;
        }

		tEFloat<T> operator/(tEFloat<T> f) const
        {
			tEFloat<T> result;
            result.m_val = m_val / f.m_val;
#ifndef NDEBUG
            result.m_perc = m_perc / f.m_perc;
#endif
            if (f.m_err.low < 0 && f.m_err.high > 0)
            {
                f.m_err.low = -inf<T>();
                f.m_err.high = inf<T>();
            }
            else
            {
                result.m_err = nextFloatInterval(m_err / f.m_err);
            }
            return result;
        }

		tEFloat<T> operator-() const
        {
			tEFloat<T> result;
            result.m_val = -m_val;
#ifndef NDEBUG
            result.m_perc = -m_perc;
#endif
            result.m_err = -m_err;
            return result;
        }

        bool operator==(tEFloat<T> f) const
        {
            return m_val == f.m_val;
        }

        T getAbsError() const
        {
            return m_err;
        }

        T upperBound() const
        {
            return m_err.high;
        }

        T lowerBound() const
        {
            return m_err.low;
        }

    private:
        T m_val;
        tInterval<T> m_err;
#ifndef NDEBUG
        long double m_perc;
#endif
    };

	template<typename T>
	tEFloat<T> operator*(tEFloat<T> fe, T f)
	{
		return fe * tEFloat<T>(f);
	}

	template<typename T>
	tEFloat<T> operator*(T f, tEFloat<T> fe)
	{
		return tEFloat<T>(f) * fe;
	}

	using EFloat = tEFloat<Float>;

	using EVec2 = glm::tvec2<EFloat>;
	using EVec3 = glm::tvec3<EFloat>;
	using EVec4 = glm::tvec4<EFloat>;
}